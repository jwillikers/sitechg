package sitechg

import (
	"context"
)

// Scanner scan's a web page for new content.
type Scanner interface {
	Scan(context.Context, string) (interface{}, error)
}
