package sitechg

import (
	"context"
	"time"

	"bitbucket.org/athrun22/scrapegoat"
)

// DynamicScanner scans a web page with dynamic content
type DynamicScanner struct {
	url        string
	scr        scrapegoat.Scraper
	wait       time.Duration
	hasChanged func(interface{}, interface{}) bool
	previous   interface{}
	firstScan  bool
}

// NewDynamicScanner creates a DynamicScanner that will scan given url with the scraper and parsing function
func NewDynamicScanner(scr scrapegoat.Scraper, wait time.Duration, hasChanged func(interface{}, interface{}) bool) *DynamicScanner {
	s := new(DynamicScanner)
	s.scr = scr
	s.wait = wait
	s.hasChanged = hasChanged
	s.firstScan = true
	return s
}

// Scan retrieves the next item if it has changed
func (s *DynamicScanner) Scan(ctx context.Context, url string) (interface{}, error) {
	var scanned interface{}
	var err error
	completed := false
	timeout := time.After(0)
	for {
		select {
		case <-ctx.Done():
			err = context.Canceled
			break
		case <-timeout:
			scanned, err = s.scr.Scrape(ctx, url)
			if err != nil {
				break
			}
			if s.firstScan || s.hasChanged(s.previous, scanned) {
				s.previous = scanned
				s.firstScan = false
				completed = true
				break
			}
			timeout = time.After(s.wait)
		}
		if err != nil || completed {
			break
		}
	}
	return scanned, err
}
