package sitechg

import (
	"context"
	"errors"
	"testing"
	"time"
)

type MockScraper struct {
	i   interface{}
	err error
}

func (s *MockScraper) Scrape(ctx context.Context, url string) (interface{}, error) {
	return s.i, s.err
}

func NewMockScraper(i interface{}, err error) *MockScraper {
	return &MockScraper{i, err}
}

func TestScan(t *testing.T) {
	t.Run("main=single", func(t *testing.T) {
		e := [2]string{"Sowing Season", "Brand New"}
		sca := NewDynamicScanner(NewMockScraper(e, nil), time.Nanosecond, hasArrayChanged)
		ctx, cancel := context.WithCancel(context.Background())
		i, err := sca.Scan(ctx, "")
		cancel()
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
		if nil == i {
			t.Error("the interface should not be nil")
		}
		s, ok := i.([2]string)
		if !ok {
			t.Error("the interface should have type string array")
		}
		if len(e) != len(s) {
			t.Errorf("the length of the array should be %d not %d", len(e), len(s))
		}
		if e[0] != s[0] {
			t.Errorf("the first string should be '%s' not '%s'", e[0], s[0])
		}
		if e[1] != s[1] {
			t.Errorf("the second string should be '%s' not '%s'", e[1], s[1])
		}
	})
	t.Run("error", func(t *testing.T) {
		sca := NewDynamicScanner(NewMockScraper(nil, errors.New("the scraper encountered an error")), time.Nanosecond, hasNotChanged)
		ctx, cancel := context.WithCancel(context.Background())
		_, err := sca.Scan(ctx, "")
		cancel()
		if nil == err {
			t.Error("there should have been an error")
		}
	})
	t.Run("cancelled", func(t *testing.T) {
		e := [2]string{"Sing Again", "Chris Walla"}
		sca := NewDynamicScanner(NewMockScraper(e, nil), time.Nanosecond, hasNotChanged)
		sca.firstScan = false
		sca.previous = e
		ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
		_, err := sca.Scan(ctx, "url")
		cancel()
		if nil == err {
			t.Error("the error should not be nil")
		}
	})
	t.Run("main=multiple", func(t *testing.T) {
		e := [2]string{"Sowing Season", "Brand New"}
		sca := NewDynamicScanner(NewMockScraper(e, nil), time.Nanosecond, hasArrayChanged)
		ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
		i, err := sca.Scan(ctx, "")
		if nil != err {
			t.Errorf("the error '%v' should be nil", err)
		}
		if nil == i {
			t.Error("the interface should not be nil")
		}
		s, ok := i.([2]string)
		if !ok {
			t.Error("the interface should contain a string array")
		}
		if len(e) != len(s) {
			t.Errorf("the length should be %d not %d", len(e), len(s))
		}
		if e[0] != s[0] {
			t.Errorf("the first string should be '%s' not '%s'", e[0], s[0])
		}
		if e[1] != s[1] {
			t.Errorf("the second string should be '%s' not '%s'", e[1], s[0])
		}
		e = [2]string{"Point Of Know Return", "Kansas"}
		sca.scr = NewMockScraper(e, nil)
		i, err = sca.Scan(ctx, "")
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
		if nil == i {
			t.Error("the interface should not be nil")
		}
		s, ok = i.([2]string)
		if !ok {
			t.Error("the interface should contain a string array")
		}
		if len(e) != len(s) {
			t.Errorf("the length should be %d not %d", len(e), len(s))
		}
		if e[0] != s[0] {
			t.Errorf("the first string should be '%s' not '%s'", e[0], s[0])
		}
		if e[1] != s[1] {
			t.Errorf("the second string should be '%s' not '%s'", e[1], s[0])
		}
		e = [2]string{"Play Crack The Sky", "Brand New"}
		sca.scr = NewMockScraper(e, nil)
		i, err = sca.Scan(ctx, "")
		cancel()
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
		if nil == i {
			t.Error("the interface should not be nil")
		}
		s, ok = i.([2]string)
		if !ok {
			t.Error("the interface should contain a string array")
		}
		if len(e) != len(s) {
			t.Errorf("the length should be %d not %d", len(e), len(s))
		}
		if e[0] != s[0] {
			t.Errorf("the first string should be '%s' not '%s'", e[0], s[0])
		}
		if e[1] != s[1] {
			t.Errorf("the second string should be '%s' not '%s'", e[1], s[0])
		}
	})
}

func hasNotChanged(x, y interface{}) bool {
	return false
}

func hasArrayChanged(x, y interface{}) bool {
	changed := false
	s1, ok := x.([2]string)
	if ok {
		s2, ok := y.([2]string)
		if ok {
			if s1 != s2 {
				changed = true
			}
		}
	}
	return changed
}
