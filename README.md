sitechg
===
sitechg, or site change, is a loose framework for monitoring web pages for updated objects of interest.

Use Case
---
Take the following as an example for when sitechg could be used. A website for a radio station displays the currently playing song. Someone wants to know what songs the radio station plays, but the website contains no such data. In order to catalogue the songs played by the website, sitechg is imported and a parsing function is implemented to parse the relevant data of the song. This data is automatically monitored for changes and when the song changes, the new song is passed back via a string slice or channel to be handled by the main program.

Example
---
An example that currently uses sitechg is the [sirius script](https://bitbucket.org/athrun22/sirius/src/develop/), for a use case such as that preceding. It uses the DynamicScanner herein which wraps chromedp to monitor a radio channel updated via javascript.

Getting Started
---
This package is a library for implementing dynamic scanning of changing web content. This section describes how to implement this library within your own go project.

### How It Works
There are three simple steps that describe the sitechg's process. They take place in the crux of sitechg, the [Scanner](./scanner.go) and are as follows.
1.  *Scrape* the html from the web page
2.  *Parse* an object from the html
3.  *Pass back* the object if it has changed since the previous retrieval

#### Scrape
There is an interface, Scraper, which implements the Scrape function that will obtain the html. A Scraper implementation using Chromedp exists in the [scraper file](./scraper.go). An implementation could easily be created using go's built-in net/http package (and probably will be in the future and included), however the chromedp Scraper will ensure dynamically loaded content is loaded onto the page.

#### Parse
Parsing is done using a parsing function that takes in the scraped html and returns an object. Parsing is left up to the implementing programmer. Additionally, chromedp has utilities that can greatly help in parsing, so implementing a new CdpScraper that implements such functionality may be of benefit. An example parsing function can be found in the [sirius package](https://bitbucket.org/athrun22/sirius/src/develop/siriusweb/parsing.go).

#### Pass Back
Pass back is how the scanner returns newly discovered objects. First, it determines if the object has changed (currently by doing a primitive equality check) and if it has changed, passes it back through a channel to be consumed by the calling function.

### Scanner
The Scanner is the crux of this library, handling the three steps above. It uses timeouts to control how often it scrapes a web page.
-   nextWait is the number of seconds to wait in between scans looking for new content.
-   parseWait is the number of seconds to wait in between bad parses (or parses that return null).

Please Use Responsibly
---
Do *NOT* use this program irresponsibly or nefariously. Using the chromedp package in the DynamicScanner with short timeouts can cause detrimental traffic on a website. Use in accordance with the website's rules and regulations.

Built With
---
-   [chromedp](https://github.com/chromedp/chromedp) - a faster, simpler way to drive browsers without external dependencies

Versioning
---
[SemVer](http://semver.org/) is used for versioning. For the versions available, see the tags on this repository.

Contributing
---
Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

Authors
---
-   Jordan Williams

License
---
This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details
